package net.dentsor.popill.comparison.utility;

public class Printer {
    public static void space() {
        space(1);
    }

    public static void space(int amount) {
        for (int i = 0; i < amount; i++) {
            println();
        }
    }

    public static void println() {
        println("");
    }

    public static void println(int output) {
        println((Object) output);
    }

    public static void println(String output) {
        println((Object) output);
    }

    public static void println(Object output) {
        System.out.println(output);
    }

    public static void print() {
        print("");
    }

    public static void print(int output) {
        print((Object) output);
    }

    public static void print(String output) {
        print((Object) output);
    }

    public static void print(Object output) {
        System.out.print(output);
    }

}
