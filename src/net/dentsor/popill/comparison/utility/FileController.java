package net.dentsor.popill.comparison.utility;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;

public class FileController {
/*
    private File file;

    public FileController(String filePath) {
        this.file = new File(filePath);
    }

    public String readFile() throws IOException {
        return readFile(this.file.getPath());
    }

    public boolean unzip(String destination) {
        return unzip(this.file.getPath(), destination, "");
    }

    public boolean unzip(String destination, String password) {
        return unzip(this.file.getPath(), destination, password);
    }

    public boolean delete() {
        if (this.file.exists()) {
            Logger.debug("File exists!");
            try {
                if (this.file.isDirectory()) {
                    Logger.debug("File is a directory!");
                    FileUtils.deleteDirectory(this.file);
                } else {
                    Logger.debug("File is not a directory!");
                    this.file.delete();
                }
            } catch (IOException ex) {
                Logger.debug("Error occurred while deleting file: '" + this.file.getPath() + "'");
            }
        } else {
            Logger.debug("File does not exist. File: '" + this.file.getPath() + "'");
        }

        if (this.file.exists())
            return false;
        else
            return true;
    }

    // ##############################
    // ###    Static functions    ###
    // ##############################
    public static String checkMD5(String logoUrl, boolean isLocalFile) {
        try {
            if (isLocalFile)
                return checkMD5(new File(logoUrl).toURI().toURL());
            else
                return checkMD5(new URL(logoUrl));
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    private static String checkMD5(URL logoUrl) {
        Logger.debug("Checking MD5 Chekcsum of: " + logoUrl);
        try {
            if (logoUrl != null) {
                MessageDigest md = MessageDigest.getInstance("MD5");
                //FileInputStream fis = new FileInputStream(logoUrl);
                InputStream fis = logoUrl.openStream();
                byte[] dataBytes = new byte[1024];

                int nread = 0;

                while ((nread = fis.read(dataBytes)) != -1) {
                    md.update(dataBytes, 0, nread);
                }

                byte[] mdbytes = md.digest();

                //convert the byte to hex format
                StringBuffer sb = new StringBuffer("");
                for (int i = 0; i < mdbytes.length; i++) {
                    sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
                }

                return sb.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static String readFile(String fileName) throws IOException {
        if (new File(fileName).exists()) {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            try {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append("\n");
                    line = br.readLine();
                }
                return sb.toString();
            } finally {
                br.close();
            }
        } else {
            throw new FileNotFoundException(fileName);
        }
    }

    public static boolean saveFileFromUrl(String destFile, String urlSource, int b) throws IOException {
        new File(destFile).getParentFile().mkdirs();
        if (new File(destFile).exists())
            if (!new File(destFile).delete())
                Logger.debug("Failed to delete old file!");

        BufferedInputStream in = null;
        FileOutputStream fout = null;
        try {
            in = new BufferedInputStream(new URL(urlSource).openStream());
            fout = new FileOutputStream(destFile);

            byte data[] = new byte[b];
            int count;
            //            int i = 0;
            while ((count = in.read(data, 0, b)) != -1) {
                //                i += count;
                //                System.out.println(count);
                //                System.out.println(i);
                fout.write(data, 0, count);
            }
        } finally {
            if (in != null)
                in.close();
            if (fout != null)
                fout.close();

            return new File(destFile).exists();
        }
    }

    public static boolean unzip(String source, String destination, String password) {
        try {
            ZipFile zipFile = new ZipFile(source);
            if (zipFile.isEncrypted()) {
                zipFile.setPassword(password);
            }
            zipFile.extractAll(destination);
            return true;
        } catch (ZipException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static int getFileSize(String fileURL) throws IOException {
        int size;
        URL url = new URL(fileURL);
        URLConnection conn = url.openConnection();
        size = conn.getContentLength();
        int ret;
        if (size < 0)
            ret = 0;
        else
            ret = size;
        conn.getInputStream().close();
        return ret;
    }

    /**
     * @param url url to read modpack JSON from
     * @return returns JSONObject of the file
     * returns null if something goes wrong
     *//*
    // TODO: Remove
    @Deprecated
    public static JSONObject addNewModpackReadFromURL(String url) throws IOException {
        if (FileController.saveFileFromUrl(Files.addNewModpackJSON(), url, Sizes.downloadAddNewModpackJson)) {
            String str = FileController.readFile(Files.addNewModpackJSON());
            JSONObject obj = new JSONObject(str);
            return obj;
        }
        return null;
    }
*/}
