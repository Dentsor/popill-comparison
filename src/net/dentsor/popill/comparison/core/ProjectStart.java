package net.dentsor.popill.comparison.core;

import net.dentsor.popill.comparison.utility.Printer;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

public class ProjectStart {
    public static void main(String[] args) {

        if (args.length < 2 || args.length > 2) {
            Printer.println("FUCK YOU, I NEED 2 ENTRIES!");
        } else {

            String dir1 = args[0];
            String dir2 = args[0];

            String[] files1 = new File(args[0]).list();
            String[] files2 = new File(args[1]).list();

            String list1 = "", list2 = "", listN = "";

            for (String entry : files1) {
                boolean match = false;
                for(String hit : files2) {
                    if (entry.equalsIgnoreCase(hit)) {
                        match = true;
                    }
                }
                if (match) {
                    listN += entry + System.getProperty("line.separator");
                } else {
                    list1 += entry + System.getProperty("line.separator");
                }
            }

            for (String entry : files2) {
                boolean match = false;
                for(String hit : files1) {
                    if (entry.equalsIgnoreCase(hit)) {
                        match = true;
                    }
                }
                if (!match) {
                    list2 += entry + System.getProperty("line.separator");
                }
            }

            Printer.println("Dir1:");
            Printer.space();
            Printer.println(list1);
            Printer.space(3);
            Printer.println("Dir2:");
            Printer.space();
            Printer.println(list2);
            Printer.space(3);
            Printer.println("DirN:");
            Printer.space();
            Printer.println(listN);
        }
    }

    public static int getFileSize(String fileURL) throws IOException {
        int size;
        URL url = new URL(fileURL);
        URLConnection conn = url.openConnection();
        size = conn.getContentLength();
        int ret;
        if (size < 0)
            ret = 0;
        else
            ret = size;
        conn.getInputStream().close();
        return ret;
    }
}
